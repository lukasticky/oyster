![Oyster Artwork by Axel Hammarbäck](https://github.com/lukas-kasticky/oyster/raw/master/OYSTER-01.png)

# Oyster
Defold module for managing global variables, gamestates and playerdata

Special thanks to [Axel Hammarbäck](https://twitter.com/AxelHammarback) for the great header image :)

[Defold Asset Portal](https://www.defold.com/community/projects/96645/)

## Installation
1. Go to this repository's [release page](https://github.com/lukas-kasticky/oyster/releases) and choose a version
2. Copy the dependency URL
3. Open your projects ```game.project``` file
4. Search for ```Dependencies``` and paste the URL
5. Done

## Usage

Add the libraries that you need to your script file:

``` Lua
local oyster = require "oyster.oyster"
local gamestate = require "oyster.gamestate"
local playerdata = require "oyster.playerdata"
local stopwatch = require "oyster.stopwatch"
```

### oyster.oyster

Add new variables:

``` Lua
oyster.add("CHUNK_SIZE", 16) -- oyster.add(NAME, VALUE)
oyster.add("WORLD_SIZE") -- VALUE is optional on initialisation
```

Remove variables:

``` Lua
oyster.remove("CHUNK_SIZE") -- oyster.remove(NAME)
```

Set values:

``` Lua
oyster.set("WORLD_SIZE", 32) -- oyster.set(NAME, VALUE)
```

Retrieve values:

``` Lua
oyster.get("WORLD_SIZE") -- oyster.get(NAME)
```

#### New in oyster 1.1

Values don't need to have the same type as the value from before:

``` Lua
oyster.set("VAR", 32)
oyster.set("VAR", "Oktocat")
```

### oyster.gamestate

___oyster 1.1 or newer required___

Standard states:

``` Lua
{
    "DEFAULT",
    "MAIN_MENU",
    "PLAY",
    "PAUSE"
}
```

Initialisation with custom states (optional):

``` Lua
local states = {"NIL", "OPTIONS", "LEVEL1"}
gamestate.init(states) -- gamestate.init(GAMESTATE_TABLE)
```

Set state:

``` Lua
gamestate.set("PLAY") -- gamestate.set(STATE)
```

Retrieve current state:

``` Lua
gamestate.get()
```

Check state:

``` Lua
gamestate.is("PAUSE") -- gamestate.is(STATE); returns boolean
```

### oyster.playerdata

___oyster 1.1 or newer required___

Add new player:

``` Lua
playerdata.add(1, {["NAME"] = "John", ["POS"] = vmath.vector3(10,10,0)}) -- playerdata.add(ID, PROPERTIES_TABLE)
playerdata.add(5) -- PROPERTIES_TABLE is optional on initialisation
```

Set player property:

``` Lua
playerdata.set(1, "NAME", "Mark") -- playerdata.set(ID, PROPERTY, VALUE)
playerdata.setAll(1, {"AGE" = 25}) -- playerdata.setAll(ID, PROPERTIES_TABLE); THIS WILL OVERWRITE ALL PROPERTIES!
```

Retrieve player property:

``` Lua
playerdata.get(1, "AGE") -- playerdata.get(ID, PROPERTY)
playerdata.getAll(1) -- playerdata.getAll(ID); returns table
```

### oyster.stopwatch

___oyster 1.2 or newer required___

Add new stopwatch:

``` Lua
stopwatch.add("playtime", true) -- stopwatch.add(ID, START)
stopwatch.add("splashscreen") -- START is optional
```

Start a stopwatch:

``` Lua
stopwatch.start("splashscreen") -- stopwatch.start(ID)
```

Retrieve elapsed time:

``` Lua
stopwatch.elapsed("splashscreen") -- stopwatch.elapsed(ID)
```

Stop a stopwatch:

``` Lua
stopwatch.stop("splashscreen") -- stopwatch.stop(ID)
```
