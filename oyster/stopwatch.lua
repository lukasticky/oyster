-- Title: Oyster - Playerdata
-- Author: Lukas Kasticky
-- Version: 1.2
-- Description: Stopwatch Defold module

local M = {}

M.watch = {}

function M.add(id, start)
    assert(type(id) == "string", "You must provide `id` of type `string` to perform `add()`")
    if start or false then
        M.watch[id] = os.time()
    else
        M.watch[id] = 0
    end
end

function M.start(id)
    assert(type(id) == "string", "You must provide `id` of type `string` to perform `start()`")
    M.watch[id] = os.time()
end

function M.elapsed(id)
    assert(type(id) == "string", "You must provide `id` of type `string` to perform `elapsed()`")
    return os.time() - M.watch[id]
end

function M.stop(id)
    assert(type(id) == "string", "You must provide `id` of type `string` to perform `stop()`")
    M.watch[id] = 99999999999
end

return M