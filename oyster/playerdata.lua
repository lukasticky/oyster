-- Title: Oyster - Playerdata
-- Author: Lukas Kasticky
-- Version: 1.2
-- Description: Defold module for managing playerdata

local M = {}

M.players = {}

function M.add(id, properties)
    assert(type(id) == "number", "You must provide `id` of type `number` to perform `add()`")
    if properties == nil then
        M.players[id] = {}
    else
	    assert(type(properties) == "table", "You must provide `properties` of type `table` to perform `add()`")
        M.players[id] = properties
    end
end

function M.set(id, property, value)
    assert(type(id) == "number", "You must provide `id` of type `number` to perform `set()`")
    assert(type(property) == "string", "You must provide `property` of type `string` to perform `set()`")
    assert(value, "You must provide `value` to perform `set()`")
    M.players[id][property] = value
end

function M.setAll(id, properties)
    assert(type(id) == "number", "You must provide `id` of type `number` to perform `setAll()`")
    assert(type(properties) == "table", "You must provide `properties` of type `table` to perform `setAll()`")
    M.players[id] = properties
end

function M.get(id, property)
    assert(type(id) == "number", "You must provide `id` of type `number` to perform `get()`")
    assert(type(property) == "string", "You must provide `property` of type `string` to perform `get()`")
    return M.players[id][property]
end

function M.getAll(id)
    assert(type(id) == "number", "You must provide `id` of type `number` to perform `getAll()`")
    return M.players[id]
end

return M