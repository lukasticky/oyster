-- Title: Oyster
-- Author: Lukas Kasticky
-- Version: 1.2
-- Description: Defold module for managing global variables

local M = {}

M.variables = {}

function M.add(name, value)
	assert(type(name) == "string", "You must provide `name` of type `string` to perform `add()`")
	table.insert(M.variables, hash(name) or name)
	M.variables[hash(name) or name] = value or nil
end

function M.remove(name)
	assert(type(name) == "string", "You must provide `name` of type `string` to perform `remove()`")
	M.variables[hash(name) or name] = nil
end

function M.set(name, value)
	assert(type(name) == "string", "You must provide `name` of type `string` to perform `set()`")
	assert(value, "You must provide `value` to perform `set()`")
	M.variables[hash(name) or name] = value
end

function M.get(name)
	assert(type(name) == "string", "You must provide `name` of type `string` to perform `get()`")
	return M.variables[hash(name) or name]
end

return M